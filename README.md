# PrivacyBox-RTPBOX

RTPBOX - Right To Privacy Box:
Tor Access Only Nextcloud Server E2EE+ Torified Router (WiFi)
many customizations.

# RTPBox

                                                                                                    RIGHT TO PRIVACY BOX                  

RTPBOX (Right To Privacy Box) is an all in one "privacy box" you can burn/flash to sdcard for your single board computer (starting with Raspberry Pi 4). If you have a great idea you can submit it and let's make something great together! More storage options in next versions.

I already got it started with soon to be released multifunctional features (an all in one privacy box!)

I don't want to give too much away too soon but you can learn more by watching this video: 
https://odysee.com/@RTP:9/wipri-spoofer-updates,-nextcloud-hidden:c

The above video demonstrates the foundation of the RTPbox (Right To Privacy Box), also known as the "Rights Box" or "Human Rights Box".

These names have not been finally settled yet and final name will be determined. For now, all are one in the same.

This is something that will continue being built up on. It won't depend on any port forwarding or anything ele.

MAIN FEATURES AS OF 3/26/2021: ALL IN ONE SIMULTANEOUSY HOSTS BOTH:

### CUSTOM TORIFIED PRIVACY ROUTER (WiFi Access): With some of my own original privacy concepts not yet released 

AND AT SAME TIME:

### CUSTOM NEXTCLOUD AS HIDDEN SERVICE (.onion): **No port forwarding/open ports required**

-This Nextcloud server is invisible to the clearnet and accessible only as an .onion with Tor browser

-Server ip protected from clients, client ip address protected from server (completely private Nextcloud server!)

-6 proxy hops between server + clients at all times

-End To End encryption between tor client and server tor client :)

-Onion address + keys generated on first login

Keep an eye on this page for more! And my Blog will have announcements: https://www.buymeacoffee.com/politictech/posts
